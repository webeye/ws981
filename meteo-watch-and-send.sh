#!/bin/bash
set -e

SSH_UPLOAD_USER="lkmi"
SSH_WORKER_HOST="a002he.webeye.services"
SPOOL_DIR=/data/spool
METEO_FILE=${SPOOL_DIR}/meteo.txt
SSH_WORKER_PORT=22

touch ${METEO_FILE}
while true
do
	inotifywait -e close_write $METEO_FILE

  dp=$(date +%Y-%m-%d)
  fp=$(date +%H-%M-%S)

  target_path=/data/lkmi/meteo/$(/bin/date +\%Y)/$(/bin/date +\%Y-\%m-\%d)
  latest_meteo=/data/${SSH_UPLOAD_USER}/web/latest-meteo.txt
  target_fname=${target_path}/$(/bin/date +\%Y-\%m-\%d-%H-%M).txt

	#ssh ${SSH_UPLOAD_USER}@${SSH_WORKER_HOST} -p ${SSH_WORKER_PORT} mkdir -p ${target_path}
	#rsync -ai -e "ssh -p ${SSH_WORKER_PORT}" ${SPOOL_DIR}/meteo.txt ${SSH_UPLOAD_USER}@${SSH_WORKER_HOST}:${target_fname}
	#ssh ${SSH_UPLOAD_USER}@${SSH_WORKER_HOST} -p ${SSH_WORKER_PORT}  "ln -sf ${target_fname} ${latest_meteo}"
	SSH_UPLOAD_USER="lkmi"
	SSH_WORKER_HOST="worker.webeye.services"
	SSH_WORKER_PORT=1222
	SSH_PRIVATE=/home/pi/.ssh/id_rsa_lkmi

	ssh ${SSH_UPLOAD_USER}@${SSH_WORKER_HOST} -i ${SSH_PRIVATE} -p ${SSH_WORKER_PORT} mkdir -p ${target_path}
	#echo "rsync -ai -e \"ssh -p ${SSH_WORKER_PORT} -i ${SSH_PRIVATE}\"  ${SPOOL_DIR}/meteo.txt ${SSH_UPLOAD_USER}@${SSH_WORKER_HOST}:${target_fname}"
	rsync -ai -e "ssh -p ${SSH_WORKER_PORT} -i ${SSH_PRIVATE}"  ${SPOOL_DIR}/meteo.txt ${SSH_UPLOAD_USER}@${SSH_WORKER_HOST}:${target_fname}
	ssh ${SSH_UPLOAD_USER}@${SSH_WORKER_HOST} -i ${SSH_PRIVATE}  -p ${SSH_WORKER_PORT}  "ln -sf ${target_fname} ${latest_meteo}"




done
