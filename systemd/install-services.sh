#!/bin/bash

base_dir=$(realpath $(dirname $0))
ln -sf ${base_dir}/webeye-make-lkmi-meteo-txt.service /etc/systemd/system/webeye-make-lkmi-meteo-txt.service
ln -sf ${base_dir}/webeye-make-lkmi-meteo-txt.timer /etc/systemd/system/webeye-make-lkmi-meteo-txt.timer
ln -sf ${base_dir}/webeye-read-serial.service /etc/systemd/system/webeye-read-serial.service

systemctl daemon-reload
systemctl enable webeye-read-serial.service
systemctl start webeye-read-serial.service
systemctl status webeye-read-serial.service

systemctl enable webeye-make-lkmi-meteo-txt.timer
systemctl start webeye-make-lkmi-meteo-txt.timer
systemctl status webeye-make-lkmi-meteo-txt.timer

