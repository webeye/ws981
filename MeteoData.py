#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""

 store information about measured values

"""

import numpy
import sched
from datetime import datetime
import time

class MeteoData(object):
    _name = ""
    _values = []
    _unit = ""

    def __init__(self, name, unit):
        self._name = name
        self._values = []
        self._unit = unit

    def value(self, val):
        self._values.append(val)

    def mean(self):
        if len(self._values):
            return numpy.mean(self._values)
        else:
            return None

    def sum(self):
        if len(self._values):
            ret_val = 0
            for v in self._values:
                ret_val += v
            return ret_val
        return None

    def max(self):
        if len(self._values):
            return numpy.max(self._values)
        else:
            return None

    def fluent(self):
        mean = self.mean()
        self._values = []
        if mean is not None:
            return {str(self._name): mean, "name": self._name, "unit": self._unit, "value": mean}

        return {}

    def fluent_sum(self):
        sum = self.sum()
        self._values = []
        if sum is not None:
            return {str(self._name): sum, "name": self._name, "unit": self._unit, "value": sum}

        return {}
