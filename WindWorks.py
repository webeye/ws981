
import math


class WindWork(object):

    @staticmethod
    def ms2tk(ms):
        """
            convert speed meters per seconds to knot
        :param ms: meters per second - wind speed
        :rtype : float
        """
        return float(ms) * 1.9438444924574

    @staticmethod
    def kt2ms(kt):
        """
            convert speed meters per seconds to knot
        :param kt: knots - wind speed
        :rtype : float
        """
        return float(kt) * 0.51444444444

    @staticmethod
    def crosswind(rnw_angle, wind_speed, wind_angle):
        """
        count side wind

        :rtype : object
        :param rnw_angle: int or float, runway angle
        :param wind_speed: float wind speed in whatever unit
        :param wind_angle: int wind angle in degrees
        :return: float side wind, greater zero is Right, bellow zero Left
        """

        # normalize angle correct wind angle to runway angle
        angle = wind_angle - rnw_angle

        radian_angle = math.radians(90 - angle)
        return round(math.cos(radian_angle) * wind_speed, 0)
