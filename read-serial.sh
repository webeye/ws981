#!/bin/bash 
base_dir=$(realpath $(dirname $0))
spool_dir="/data/spool/src"

mkdir -p $spool_dir
. ${base_dir}/venv/bin/activate

stty -F /dev/ttyUSB0 raw speed 57600

cat /dev/ttyUSB0 | while read ln 
	do
	  echo $ln;
	  IFS=/ read code vname val unit <<< $(echo "$ln" | python ${base_dir}/parser.py)
    if [[ "$vname" == "" ]]; then
      continue
    fi
	  echo "Name: $vname, Value: $val, Unit: $unit"
	  val=$(echo $val | tr -d "[]")

	  if [[ "$vname" != "" ]]; then
		  echo $val > ${spool_dir}/${vname}
	  fi
	  redis_key=$vname
	  #redis_key=$(echo "$(echo $vname | tr \".\" \"_\")$(date +%H%M)")
	  echo $redis_key
	  echo "XADD $redis_key * $vname $val" | redis-cli
          echo "XTRIM $redis_key MAXLEN 100" | redis-cli
	done
