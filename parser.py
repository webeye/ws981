import sys
import argparse
from wscom import ws981

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input-file', type=argparse.FileType('r', errors='replace', encoding='ASCII'), default=(None if sys.stdin.isatty() else sys.stdin))
    opt = parser.parse_args()
    while True:
        ln = opt.input_file.readline().strip()
        if not ln or ln is None:
            break

        print(ln)
        for respln in ws981().parse_line(ln):
            print(f"{ln}/{respln._name}/{respln._values}/{respln._unit}")
