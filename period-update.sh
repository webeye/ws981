#!/bin/bash
base_dir=$(realpath $(dirname $0))

spool_dir=/data/spool/src


find /data/spool/src/ -type f -mmin +20 -delete


. ${base_dir}/venv/bin/activate


#echo "python ${base_dir}/wind-variable.py $(cat ${spool_dir}/wind_movAvg_2m_dir) ${spool_dir}/wind_var_min ${spool_dir}/wind_var_max"
if [[ -f ${spool_dir}/wind_movAvg_2m_dir ]]; then
  python ${base_dir}/wind-variable.py $(cat ${spool_dir}/wind_movAvg_2m_dir) ${spool_dir}/wind_var_min ${spool_dir}/wind_var_max > /dev/null
fi

python ${base_dir}/wind-speed.py ${spool_dir}/wind_actual_gust_kt ${spool_dir}/wind_actual_gust_ms ${spool_dir}/wind_actual_gust_title ${spool_dir}/wind_actual_speed_kt ${spool_dir}/wind_actual_speed_ms > /dev/null




if [[ -f  ${spool_dir}/wind_actual_dir ]]; then

python ${base_dir}/runway-select.py  $(cat ${spool_dir}/wind_actual_dir) $(cat ${spool_dir}/wind_actual_speed) ${spool_dir}/runway_data > /dev/null
fi
#python ${base_dir}/wind-speed-rose.py ${spool_dir}/wind_rose

echo $(echo "{"
ls -1 $spool_dir/| while read ln; do
  if [[ "$ln" == "runway_data" ]] ; then
    echo "\"$ln\": $(cat $spool_dir/$ln),";
  elif [[ "$ln" == "wind_rose" ]] ; then
    echo "\"$ln\": $(cat $spool_dir/$ln),";
  else
    echo "\"$(echo $ln | tr '.' '_')\": \"$(cat $spool_dir/$ln)\",";
 fi
done
echo "\"dt_local\":\"$(date +'%H:%M %d.%m.%Y') CEST\","
echo "\"dt_utc\": \"$(date -u +'%H:%M %d.%m.%Y') UTC\"" 

echo "}")
