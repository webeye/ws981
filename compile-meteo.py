import argparse
import sys
import jinja2
import json

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='compile meteo data template')
    parser.add_argument('template', type=argparse.FileType('r'), default="meteo.j2")
    parser.add_argument('data', type=argparse.FileType('r'), default=sys.stdin)
    args = parser.parse_args()

    tmpl = args.template.read()
    context = json.load(args.data)
    context['wind_wad_movAvg_2m_speed'] = float(context.get('wind_wad_movAvg_2m_speed', 0))
    context['wind_actual_gust_kt'] = float(context.get('wind_actual_gust_kt', 0))
    context['wind_actual_speed_ms'] = float(context.get('wind_actual_speed_ms', 0))
    context['wind_actual_speed_kt'] = float(context.get('wind_actual_speed_kt', 0))
    # print(context)

    template = jinja2.Template(tmpl)

    print(template.render(context))
