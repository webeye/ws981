#!/usr/bin/env python
# -*- coding: utf-8 -*-


import logging
from MeteoData import MeteoData

logging.basicConfig(level=logging.INFO)


class Ws981Exception(Exception):
    pass


class ws981(object):
    tr_code = {
        "p": 0x0,
        "a": 0x1,
        "b": 0x2,
        "S": 0x3,
        "d": 0x4,
        "U": 0x5,
        "V": 0x6,
        "G": 0x7,
        "h": 0x8,
        "Y": 0x9,
        "Z": 0xA,
        "K": 0xB,
        "\\": 0xC,
        "M": 0xD,
        "N": 0xE,
        "?": 0xF
    }

    _data = {}
    def __init__(self):
        self._data = {}
        self._codes = {
            "G": self.parse_g,
            "H": self.parse_h,
            "I": self.parse_h,
            "L": self.parse_l,
            "M": self.parse_m,
            "W": self.parse_w,
            "X": self.parse_x,
            "Y": self.parse_y,
            "A": self.parse_a,
            "B": self.parse_b,
            "C": self.parse_c,
            "D": self.parse_d,
            # "Q": self.parse_q
        }


    def wind_value(self, v, anot):
        # print('WindVal', v)
        w_sp = v.split(" ")
        # print(w_sp)
        if len(w_sp) == 2:
            wind_dir = self._data.get(u"%s_dir" % anot, MeteoData(u"%s_dir" % anot, "°"))
            wind_dir.value(int(u"%s0" % w_sp[0]))
            self._data[u"%s_dir" % anot] = wind_dir
            yield wind_dir
            wd_val = w_sp[1]
        elif len(w_sp) == 3:
            wind_dir = self._data.get(u"%s_dir" % anot, MeteoData(u"%s_dir" % anot, "°"))
            wind_dir.value(int(u"%s0" % w_sp[0]))
            self._data[u"%s_dir" % anot] = wind_dir
            yield wind_dir
            wd_val = w_sp[2]
        else:
            wd_val = w_sp[4]

        wind_speed = self._data.get(u"%s_speed" % anot, MeteoData(u"%s_speed" % anot, "m/s"))
        wind_speed.value(float(wd_val[0]) / 10.0)
        self._data[u"%s_speed" % anot] = wind_speed
        yield wind_speed

    def parse_g(self, v):
        for g in   self.wind_value(v, u"wind_actual"):
            yield g

    def parse_h(self, v):
        for h in self.wind_value(v, u"wind_movAvg_2m"):
            yield h

    def parse_i(self, v):
        for i in  self.wind_value(v, u"wind_movAvg_10m"):
            yield i

    def parse_l(self, v):
        d = self._data.get(u"dew_point", MeteoData(u"dew_point", u"°C"))
        d.value(int(v) / 10.0)
        self._data[u"wind"] = d
        yield d

    def parse_m(self, v):
        d = self._data.get(u"pressure_trend", MeteoData(u"pressure_trend", u""))
        d.value(int(v) / 10.0)
        self._data[u"pressure_trend"] = d
        yield d

    def wind_wad_walue(self, v, anot):
        da_dir = self._data.get(u"%s_speed" % anot, MeteoData(u"%s_speed" % anot, "m/s"))
        da_speed = self._data.get(u"%s_direction" % anot, MeteoData(u"%s_dir" % anot, u"°"))
        # XaVpaVS
        a = self.tr_code[v[2:3]]
        b = self.tr_code[v[3:4]]
        c = self.tr_code[v[4:5]]

        if len(v) == 6:
            d = self.tr_code[v[5:6]]
        else:
            d = 0
        e = self.tr_code[v[0:1]]
        f = self.tr_code[v[1:2]]

        tdir = (256 * (16 * (a & 0x0F)) + (b & 0x0F) + 16 * (c & 0x0F) + (d & 0x0F)) / 37.38932004
        tdir = tdir*10
        da_dir.value(tdir)
        yield da_dir

        speed = (10 * (16 * (e & 0x0F)) + (f & 0x0F))
        da_speed.value(speed)
        yield da_speed
        self._data[u"%s_direction" % anot] = da_dir
        self._data[u"%s_speed" % anot] = da_speed

    def parse_w(self, v):
        for w in  self.wind_wad_walue(v, u"wind_wad_actual"):
            yield w

    def parse_x(self, v):
        for x in self.wind_wad_walue(v, u"wind_wad_movAvg_2m"):
            yield x

    def parse_y(self, v):
        for y in  self.wind_wad_walue(v, u"wind_wad_movAvg_10m"):
            yield y

    def parse_a(self, v):
        d = self._data.get(u"temperature", MeteoData(u"temperature", u"°C"))
        d.value(int(v) / 10.0)
        self._data[u"temperature"] = d
        # statsd.gauge(u"meteo.temperature", int(v)/10.0)
        yield d

    def parse_b(self, v):
        d = self._data.get(u"humidity", MeteoData(u"humidity", u"%"))
        d.value(int(v) / 10.0)
        self._data[u"humidity"] = d
        # statsd.gauge(u"meteo.humidity", int(v)/10.0)
        yield d

    def parse_c(self, v):
        d = self._data.get(u"pressure_land", MeteoData(u"pressure_land", u"hPa"))
        d.value(int(v) / 10.0)
        self._data[u"pressure.land"] = d
        # statsd.gauge(u"meteo.pressure.land", int(v)/10.0)
        yield d

    def parse_d(self, v):
        # local pressure on field elevation
        d = self._data.get(u"pressure_sea", MeteoData(u"pressure_sea", u"hPa"))
        d.value(int(v) / 10.0)
        self._data[u"pressure_sea"] = d
        # statsd.gauge(u"meteo.pressure.sea", int(v)/10.0)
        yield d

    def parse_e(self, v):

        d = self._data.get(u"unknown", MeteoData(u"unknown", u"unknown"))
        d.value(int(v) / 10.0)
        self._data[u"unknown"] = d

        yield d

    def parse_q(self, v):
        yield None

    def parse_line(self, l: str) -> MeteoData:
        ret_val = None
        if l == "":
            yield ret_val
        s = l[0]
        # parsed = False

        if s in self._codes:
            for y in self._codes[s](l[1:]):
                if y is None:
                    continue
                yield y
            # parsed = True

        # if parsed is False:
        #     raise Ws981Exception("not parsed: %s" % l)


